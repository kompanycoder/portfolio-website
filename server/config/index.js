//  server port
const Port = process.env.PORT || 5001;
// baseUrl
const baseUrl = 'http://localhost:5001/';

// export the needed files
module.exports = {
    Port,
    baseUrl
};

