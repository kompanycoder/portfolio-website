//  declare a skills schema
const mongoose = require('mongoose');
const SkillSchema = mongoose.Schema;

const SkillModel = SkillSchema({
    title: {
        type: String,
        required: true
    },
    rating: {
        type: Number,
        required: true,
        min: 0,
        max: 9
    },
    proficiency: {
        type: String,
        required: true
    },
    demand: {
        type: String,
        required: true
    }
}, {timestamps: true});

const Skill = mongoose.model('Skill', SkillModel);

module.exports = {
    Skill
};