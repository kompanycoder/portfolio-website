const mongoose = require('mongoose');
const ProjectSchema = mongoose.Schema;

const ProjectModel = ProjectSchema({
    title: {
        required: true,
        type: String,
        max: 200
    },
    description: {
        required: true,
        type: String,
        max: 100
    },
    image: {
        type: String
    },
    index: {
        type: Number,
        default: 0
    }
}, {
    timestamps: true
});

const Project = mongoose.model('Project', ProjectModel);

module.exports = {
    Project
}