// require needed dependencies
const Express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
// require more config options
// logic for getting env function
const { Port, baseUrl } = require("./config");

// log just to be sure;
// console.log(Port, baseUrl);

// init express app
const app = Express();

// use morgan for requests logs
app.use(morgan("dev"));

// pase data to json format
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

// use mongoose for a new connection
mongoose.Promise = global.Promise;
const mongoOptions = {
  useNewUrlParser: true
};
// require mongoose models
// const { Project } = require("./models/project");
// const { Skill } = require("./models/skill");
// console.log(Project);
// console.log(Skill);
mongoose.connect(baseUrl, mongoOptions, () => {
  console.log("Connection Sussessfully to personalDb.");
});

app.get("/", (req, res) => {
  res.status(200).json({
    Message: " Welcome to the backend portfolio site"
  });
});

app.listen(Port, () => {
  console.log("Portfolio server running on port: " + Port);
});
