import React, { Component } from "react";
import "./App.scss";
import Layout from "./components/layout";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faLaptopCode } from "@fortawesome/free-solid-svg-icons";

library.add(faLaptopCode);

class App extends Component {
  render() {
    return (
      <div className="App">
        <Layout />
      </div>
    );
  }
}

export default App;
