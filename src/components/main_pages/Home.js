import React, { Component } from "react";

export default class Home extends Component {
  render() {
    return (
      <div className="container-hold">
        <div className="back-home" />
        <div className="home">
          <h1 className="intro-title">
            <span className="highlight">Fullstack</span> <span className="highlight">Developer</span><span className="highlight">&</span> 
             <span className="highlight">javascript</span> <span className="highlight">addict.</span>
          </h1>
          <div className="paragraph">
            <a className="btn btn-outline btn-primary" href="/projects"> See More</a>
            <a className="btn btn-outline btn-success" href="/contact"> Reach Me</a>
          </div>
        </div>
      </div>
    );
  }
}
