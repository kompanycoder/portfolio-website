import React, { Component } from "react";

export default class about extends Component {
  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
    this.renderSkills = this.renderSkills.bind(this);
    this.state = {
      skills: [
        "react",
        "vue",
        "javascript",
        "sql",
        "posgresDb",
        "mongoDb",
        "node",
        "express",
        "html",
        "css",
        "bootstrap"
      ],
      contentAbout: "Proficient in React, Vue, Javascript and Python."
    };
  }
  renderSkills(skills) {
    console.log(skills);
    return skills.length
      ? 
       skills.forEach(skill => {
          return <li> {skill} </li>;
        })
      
      : null;
  }
  

  render(props, state) {
    console.log(this.props);
    console.log(
      "about page props ..." + this.state.contentAbout + this.state.skills
    );
    const skillsArray = this.state.skills;
    console.log(skillsArray);
    return (
      <div className="container-hold">
        <div className="about">
          <h2 className="headine-2"> About me</h2>
          <div className="content">
            <h6>{this.state.contentAbout} </h6>
            <h6>skills: </h6>
            {/* to do render skills array properly */}
            {this.renderSkills(skillsArray)}
          </div>
          {/* component for my skill points here */}
        </div>
      </div>
    );
  }
}
