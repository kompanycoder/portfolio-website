import React, { Component } from "react";
import { BrowserRouter as Router, Link } from "react-router-dom";
import { Collapse, Navbar, NavbarToggler, Nav, NavItem } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ContentLinks from "./ContentLinks";

export class Header extends Component {
  constructor(props) {
    super(props);

    this.navToggle = this.navToggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  navToggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return (
        <Router> 
          <Navbar color="dark" dark expand="md" className="Navbar">
            <Link className="NavBrand" to="/">
              <FontAwesomeIcon icon="laptop-code" className="logo"/> <span className="text-center">KompanyCoder</span>
            </Link>
            <NavbarToggler onClick={this.navToggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                <NavItem className="NavItem">
                  <Link to="/about">About</Link>
                </NavItem>
                <NavItem className="NavItem">
                  <Link to="/works">Work & Interests</Link>
                </NavItem>
                <NavItem className="NavItem">
                  <Link to="/projects">Sample Projects</Link>
                </NavItem>
                <NavItem className="NavItem">
                  <Link to="/contact">Contact</Link>
                </NavItem>
              </Nav>
            </Collapse>
          </Navbar>

          <ContentLinks />
        </Router>
    );
  }
}

export default Header;
