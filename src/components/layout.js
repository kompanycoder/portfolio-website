import React from "react";
import Header from '../components/header';
import Footer from '../components/footer';

const layout = (props) => {
    
  return (
    <div>
      <Header />
      {/* props stand for react routr dom manipulation */}
        {props.children}
      <Footer />
    </div>
  );
};

export default layout;
