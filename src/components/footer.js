import React, { Component } from "react";
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <div className="icons">
          {/* Implement font awesome social icons here.*/}
          linkedIn,Github,Twitter fontawesome icons.
        </div>
        <div className="copyright">
            <span> Copyright &copy; 2019. <Link className="footerLink" to="/">
              <FontAwesomeIcon icon="laptop-code" className="logo"/> <span className="text-center">KompanyCoder</span>
            </Link>.</span>
        </div>
      </div>
    );
  }
}

export default Footer;
