import React, { Component } from "react";
import Home from "./main_pages/Home";
import About from "./main_pages/About";
import Works from "./main_pages/Works";
import Contact from "./main_pages/Contact";
import Projects from "./main_pages/Projects";
import { Route } from "react-router-dom";

export class ContentLinks extends Component {
  render() {
    // console.log(props);
    return (
      <div className="router-view" id="router">
        <Route exact path="/" component={Home} />
        <Route exact path="/about" component={About} />
        <Route exact path="/works" component={Works} />
        <Route exact path="/projects" component={Projects} />
        <Route exact path="/contact" component={Contact} />
      </div>
    );
  }
}

export default ContentLinks;
